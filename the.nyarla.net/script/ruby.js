"use strict";
(function () {

    var MarkupRuby = function ( str, captured, offset, s ) {
        var strLst = captured.split('|');
        var target = strLst.shift().split('');
        var ruby   = strLst;
    
        var ret    = '<ruby>';
        if ( target.length == ruby.length ) {
            for ( var i = 0, len = target.length; i < len; i++ ) {
                ret = ret + target[i] + '<rt>' + ruby[i] + '</rt>';
            }
        }
        else {
            ret = ret + target.join('') + '<rt>' + ruby.join('') + '</rt>';
        }
        ret = ret + '</ruby>';
    
        return ret;
    }

    var ReplaceHandler = function () {
        var lst = document.getElementsByTagName('p');
        for ( var i = 0, len = lst.length; i < len; i++ ) {
            lst[i].innerHTML = (new String(lst[i].innerHTML)).replace(/[{](.+?)[}]/g, MarkupRuby);
        }
    }

    window.addEventListener('DOMContentLoaded', ReplaceHandler, false);

})();
