/*
amazon.js - replace amazon link to amazon widget
================================================

*/

"use strict";
(function  () {
    var AssociateTag = 'nyalranet-22';
    var ImagePrefix  = 'http://ec2.images-amazon.com/images/P/';
    var CaptureRe    = /^http:\/\/www\.amazon\.co\.jp\/gp\/product\/([A-Z0-9]{10}).*?(?:[#](l|m|s)[:]([a-z]+))$/;
    var SizeMap      = {
          's':'THUMBZZZ'
        , 'm':'MZZZZZZZ'
        , 'l':'LZZZZZZZ'
    };

    var BuildImageURL = function ( asin, size ) {
        if (! size)
            size = 'm';

        return ImagePrefix + asin + '.01.' + size + '.jpg';
    }

    var BuildAssociateURL = function ( asin ) {
        return 'http://www.amazon.co.jp/gp/product/' + asin + '?tag=' + AssociateTag;
    }

    var BuildImageLink  = function ( asin, title, size ) {
        var imgSrc = BuildImageURL( asin, size ),
            href   = BuildAssociateURL( asin ),
            img    = document.createElement('img'),
            link   = document.createElement('a');
    
        img.setAttribute( 'src', imgSrc );
        img.setAttribute( 'title', title );
        img.setAttribute( 'alt',   title );

        link.setAttribute( 'href', href );
        link.appendChild( img );

        return link;
    }

    var BuildTextLink  = function ( asin, title ) {
        var href = BuildAssociateURL( asin ),
            link = document.createElement('a'),
            text = document.createTextNode(title);

        link.setAttribute('href', href);
        link.appendChild(text);

        return link;
    }

    var FrameBuilders = {
          'figure': function ( asin, title, size ) {
                var container = document.createElement('figure'),
                    thumbnail = document.createElement('p'),
                    label     = document.createElement('figcaption');

                thumbnail.className = 'thumbnail';
                thumbnail.appendChild( BuildImageLink( asin, title, size ) );

                label.appendChild( BuildTextLink( asin, title ) );

                container.className = 'item-figure'
                container.appendChild( thumbnail );
                container.appendChild( label );

                return container;
          }
        , 'detail': function ( asin, title, size ) {
                var table      = document.createElement('table'),

                    thumbCell  = document.createElement('td'),
                    thumbLink  = BuildImageLink( asin, title, size ),

                    titleRow   = document.createElement('tr'),
                    tLabelCell = document.createElement('th'),
                    tLabel     = document.createTextNode('タイトル'),
                    tLinkCell  = document.createElement('td'),
                    tLink      = BuildTextLink( asin, title ),

                    asinRow    = document.createElement('tr'),
                    aLabelCell = document.createElement('th'),
                    aLabel     = document.createTextNode('ASIN'),
                    aTextCell  = document.createElement('td'),
                    aText      = document.createTextNode(asin),

                    shopRow    = document.createElement('tr'),
                    sLabelCell = document.createElement('th'),
                    sLabel     = document.createTextNode('購入'),
                    sLinkCell  = document.createElement('td'),
                    sLink      = BuildTextLink( asin, 'Amazonで買う！' );

                    thumbCell.setAttribute('rowspan', 3);
                    thumbCell.appendChild(thumbLink);

                    tLabelCell.appendChild(tLabel);
                    tLinkCell.appendChild(tLink);

                    titleRow.appendChild(thumbCell);
                    titleRow.appendChild(tLabelCell);
                    titleRow.appendChild(tLinkCell);

                    table.appendChild(titleRow);

                    aLabelCell.appendChild(aLabel);
                    aTextCell.appendChild(aText);

                    asinRow.appendChild(aLabelCell);
                    asinRow.appendChild(aTextCell);
                    table.appendChild(asinRow);

                    sLabelCell.appendChild(sLabel);
                    sLinkCell.appendChild(sLink);

                    shopRow.appendChild(sLabelCell);
                    shopRow.appendChild(sLinkCell);

                    table.appendChild(shopRow);

                    table.className = 'item-detail'

                    return table;
          }
    }

    var ReplaceHandler = function () {
        var lst = document.querySelectorAll('p > a[href^="http://www.amazon.co.jp/gp/product/"]:only-child');

        LST: for ( var i = 0, len = lst.length; i < len; i++ ) {
            var elm     = lst[i];
            var href    = elm.getAttribute('href');
            var title   = elm.textContent;
            var capture = CaptureRe.exec(href);

            if ( ! capture )
                continue LST;

            var asin    = capture[1];
            var size    = ( capture[2] || 'm' );
            var frame   = ( capture[3] || 'detail');
            var builder = null;

            if ( frame in FrameBuilders ) {
                builder = FrameBuilders[frame]
            }
            else {
                builder = FrameBuilder['detail']
            }

            elm.parentNode.parentNode.replaceChild(
                builder( asin, title, size ),
                elm.parentNode
            )
        }
    }

    window.addEventListener('DOMContentLoaded', ReplaceHandler, false );

})()

