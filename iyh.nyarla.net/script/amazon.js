/*
amazon.js - replace amazon link to amazon widget
================================================

*/

"use strict";
(function () {
    var AssociateTag = 'nyalranet-22';

    var SizeMap = {
          's':'THUMBZZZ'
        , 'm':'MZZZZZZZ'
        , 'l':'LZZZZZZZ'
    };
    var ImagePrefix = 'http://ec2.images-amazon.com/images/P/'; 
    var LinkRe      = /^http:\/\/www\.amazon\.co\.jp\/gp\/product\/([A-Z0-9]{10}).*?(?:[#](l|m|s)[:]([a-z]+))$/;

    var ItemBuilder = function ( asin, title, url, size, style ) {
        if ( ! size )
            size    = 'm'
        if ( ! style )
            style   = 'float';

        var ImageSize = SizeMap[size];

        var container = document.createElement('figure');
        var caption   = document.createElement('figcaption');

        var imageSrc  = ImagePrefix + asin + '.01.' + ImageSize + '.jpg';
        var image     = document.createElement('img');
            image.setAttribute('src', imageSrc);
            image.setAttribute('title', title);
            image.setAttribute('alt', "Thumbnail");

        var imageLink = document.createElement('a');
            imageLink.setAttribute('href', url);
            imageLink.appendChild( image );

        var titleLink = document.createElement('a');
            titleLink.setAttribute('href', url);
            titleLink.appendChild( document.createTextNode(title) );

        caption.appendChild( titleLink );

        var embed     = document.createElement('p');
            embed.className = 'thumbnail';
            embed.appendChild(imageLink);

        container.appendChild( embed );
        container.appendChild( caption );

        container.className = style;

        return container;
    }

    var ReplaceHandler = function () {
        var lst = document.querySelectorAll('p > a[href^="http://www.amazon.co.jp/gp/product/"]:only-child');
        for ( var i = 0, len = lst.length; i < len; i++ ) {
            var elm     = lst[i];
            var href    = elm.getAttribute('href');
            var capture = LinkRe.exec(href); 
            var asin    = capture[1];
            var size    = capture[2] || 'm';
            var style   = capture[3] || 'float';
            var link    = 'http://www.amazon.co.jp/gp/product/' + asin + '?tag=' + AssociateTag ;
            var title   = elm.textContent;

            elm.parentNode.parentNode.replaceChild(
                ItemBuilder( asin, title, link, size, style ),
                elm.parentNode
            );
        }
    }

    if ( window.addEventListener ) {
        window.addEventListener('load', ReplaceHandler, false);
    }
})();

